<?php

/* 
 * 2017-01-18
 * @author Weerayut Namwongsa <weerayut.code37@gmail.com>
 */

use yii\web\AssetBundle;

class MaterialAsset extends AssetBundle{
    public $sourcePath="@vendor/hris/yii2-theme-material/asset";
    public $baseUrl = "@web";
    
    public $css = [
        'css/bootstrap-material-design.min.css',
        'css/ripples.min.css',
    ];
    public $js = [
        'js/material.min.js',
        'js/repples.min.js',
    ];
    public $depends = [
        'yii/web/YiiAsset',
        'yii/bootstrap\BootstrpAsset',
    ];
    public function init(){
        parent::init();
    }
}

