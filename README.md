Yii2 Material Desidn and Bootstrap Theme
=============================
Theme for Yii2 Web Application

Installation
-------------

```
php composer.phar require --prefer-dist hris/yii2-theme-material "*"
```

or add

```
"hris/yii2-theme-material": "*"
```

to the require section of your composer.json file.

Usage
-----

Open your layout views/layours/main.php and add

```
use hris\theme\material;

material\MaterialAsset::register($this);
```
